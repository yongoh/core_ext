require 'spec_helper'

describe CoreExt::Hash do
  describe "#html_attribute_merge" do
    subject do
      owner.html_attribute_merge(other)
    end

    context "引数にハッシュを渡した場合" do
      let(:owner){ {id: "ID", name: "Name", class: [""], data: {hoge: nil}} }
      let(:other){ {name: "名前", for: "ふぉー", class: [nil], data: {piyo: ""}} }

      it "は、自身を変化させないこと" do
        subject
        expect(owner).to match({id: "ID", name: "Name", class: [""], data: {hoge: nil}})
      end

      context "キーワード引数`:compact`に`true`を渡した場合 (default)" do
        it "は、渡した方を優先してマージし、キー`:class`,`:data`が空の値しか含まない場合はそれを持たないハッシュを返すこと" do
          is_expected.to match({id: "ID", name: "名前", for: "ふぉー"})
        end
      end

      context "キーワード引数`:compact`に`false`を渡した場合" do
        subject do
          owner.html_attribute_merge(other, compact: false)
        end

        it "は、渡した方を優先してマージし、キー`:class`,`:data`を保持したハッシュを返すこと" do
          is_expected.to match({id: "ID", name: "名前", for: "ふぉー", class: ["", nil], data: {hoge: nil, piyo: ""}})
        end
      end
    end

    describe "HTMLクラス属性" do
      context "自身がキー`:class`を持たない場合" do
        let(:owner){ {} }

        context "キー`:class`に`nil`を持つハッシュを渡した場合" do
          let(:other){ {class: nil} }
          it{ is_expected.to match({}) }
        end

        context "キー`:class`に文字列を持つハッシュを渡した場合" do
          let(:other){ {class: "foo"} }

          it "は、キー`:class`に渡した文字列を含む配列を持つハッシュを返すこと" do
            is_expected.to match({class: ["foo"]})
          end
        end

        context "キー`:class`に配列を持つハッシュを渡した場合" do
          let(:other){ {class: %w(foo bar)} }

          it "は、キー`:class`に渡した配列を持つハッシュを返すこと" do
            is_expected.to match({class: ["foo", "bar"]})
          end
        end
      end

      context "自身のキー`:class`の値が文字列の場合" do
        let(:owner){ {class: "hoge"} }

        context "キー`:class`に`nil`を持つハッシュを渡した場合" do
          let(:other){ {class: nil} }

          it "は、キー`:class`に元の文字列のみを含む配列を持つハッシュを返すこと" do
            is_expected.to match({class: ["hoge"]})
          end
        end

        context "キー`:class`に文字列を持つハッシュを渡した場合" do
          let(:other){ {class: "foo"} }

          it "は、キー`:class`に元の文字列と渡した文字列を含む配列を持つハッシュを返すこと" do
            is_expected.to match({class: ["hoge", "foo"]})
          end
        end

        context "キー`:class`に配列を持つハッシュを渡した場合" do
          let(:other){ {class: %w(foo bar)} }

          it "は、キー`:class`に元の配列と渡した配列を連結したものを持つハッシュを返すこと" do
            is_expected.to match({class: ["hoge", "foo", "bar"]})
          end
        end
      end

      context "自身のキー`:class`の値が配列の場合" do
        let(:owner){ {class: %w(hoge piyo)} }

        context "キー`:class`に`nil`を持つハッシュを渡した場合" do
          let(:other){ {class: nil} }

          it "は、キー`:class`に元と同じ値の配列を持つハッシュを返すこと" do
            is_expected.to match({class: ["hoge", "piyo"]})
          end
        end

        context "キー`:class`に文字列を持つハッシュを渡した場合" do
          let(:other){ {class: "foo"} }

          it "は、キー`:class`に元の配列と渡した文字列を含む配列を持つハッシュを返すこと" do
            is_expected.to match({class: ["hoge", "piyo", "foo"]})
          end
        end

        context "キー`:class`に配列を持つハッシュを渡した場合" do
          let(:other){ {class: %w(foo bar)} }

          it "は、キー`:class`に元の配列と渡した配列を連結したものを持つハッシュを返すこと" do
            is_expected.to match({class: ["hoge", "piyo", "foo", "bar"]})
          end
        end
      end

      context "キー`:class`の値がハッシュの場合" do
        let(:owner){ {class: {hoge: "Hoge", piyo: "Piyo"}} }
        let(:other){ {class: {foo: "Foo", bar: "Bar"}} }

        it "は、ハッシュを配列化して連結化したものをキー`:class`に持つハッシュを返すこと" do
          is_expected.to match({class: [[:hoge, "Hoge"], [:piyo, "Piyo"], [:foo, "Foo"], [:bar, "Bar"]]})
        end
      end
    end

    describe "HTMLデータ属性" do
      context "自身がキー`:data`を持たない場合" do
        let(:owner){ {} }
        let(:other){ {data: {foo: "Foo", bar: "Bar"}} }

        it "は、キー`:data`に渡したものと同じ値のハッシュを持つハッシュを返すこと" do
          is_expected.to match({data: {foo: "Foo", bar: "Bar"}})
        end
      end

      context "自身のキー`:data`がハッシュの場合" do
        let(:owner){ {data: {hoge: "Hoge", piyo: "Piyo"}} }
        let(:other){ {data: {foo: "Foo", bar: "Bar", piyo: "ぴよ"}} }

        it "は、キー`:data`に渡した方を優先してマージしたハッシュを持つハッシュを返すこと" do
          is_expected.to match({data: {hoge: "Hoge", foo: "Foo", bar: "Bar", piyo: "ぴよ"}})
        end
      end

      context "キー`:data`にハッシュ以外の値を持つハッシュを渡した場合" do
        let(:owner){ {} }

        it{ expect{ owner.html_attribute_merge(data: nil) }.to raise_error(TypeError) }
        it{ expect{ owner.html_attribute_merge(data: ["foo", "bar"]) }.to raise_error(TypeError) }
      end
    end
  end

  describe "#html_attribute_merge!" do
    subject do
      owner.html_attribute_merge!(other)
    end

    let(:owner){ {id: "ID", name: "Name", class: ["hoge", "piyo"], data: {hoge: "Hoge", piyo: "Piyo"}} }
    let(:other){ {name: "名前", for: "ふぉー", class: ["foo", "bar"], data: {foo: "Foo", bar: "Bar", piyo: "Piyo"}} }

    it "は、渡したハッシュを自身に破壊的にマージすること" do
      subject
      expect(owner).to match({
        id: "ID", name: "名前", for: "ふぉー",
        class: ["hoge", "piyo", "foo", "bar"],
        data: {hoge: "Hoge", foo: "Foo", bar: "Bar", piyo: "Piyo"},
      })
    end

    it "は、自身を返すこと" do
      is_expected.to equal(owner)
    end
  end

  describe "#url_parameter_merge" do
    subject do
      owner.url_parameter_merge(other)
    end

    context "自身も引数もキー`:q`を持たない場合" do
      let(:owner){ {a: "A1", b: "B1", q: {hoge: nil}} }
      let(:other){ {b: "B2", c: "C2", q: {piyo: ""}} }

      it "は、自身を変化させないこと" do
        subject
        expect(owner).to match({a: "A1", b: "B1", q: {hoge: nil}})
      end

      context "キーワード引数`:compact`に`true`を渡した場合 (default)" do
        it "は、渡した方を優先してマージし、キー`:q`が空の値しか含まない場合はそれを持たないハッシュを返すこと" do
          is_expected.to match({a: "A1", b: "B2", c: "C2"})
        end
      end

      context "キーワード引数`:compact`に`false`を渡した場合" do
        subject do
          owner.url_parameter_merge(other, compact: false)
        end

        it "は、渡した方を優先してマージし、キー`:q`に空のハッシュを持つハッシュを返すこと" do
          is_expected.to match({a: "A1", b: "B2", c: "C2", q: {hoge: nil, piyo: ""}})
        end
      end
    end

    context "自身も引数もキー`:q`の値にハッシュを持つ場合" do
      let(:owner){ {a: "A1", b: "B1", q: {medium_eq: "Anime", date_lt: "2019-01-07"}} }
      let(:other){ {b: "B2", c: "C2", q: {medium_eq: "Manga", date_gteq: "2018-12-25"}} }

      it "は、キー`:q`の値であるハッシュもマージすること" do
        is_expected.to match({
          a: "A1", b: "B2", c: "C2",
          q: {medium_eq: "Manga", date_lt: "2019-01-07", date_gteq: "2018-12-25"},
        })
      end
    end

    context "キーワード引数`:query_key`に検索クエリキーを渡した場合" do
      subject do
        owner.url_parameter_merge(other, query_key: "search")
      end

      let(:owner){ {a: "A1", b: "B1", q: "Q1", search: {title_cont: "Sword", episode_gt: 5}} }
      let(:other){ {b: "B2", c: "C2", q: "Q2", search: {title_cont: "Art", episode_lteq: 12}} }

      it "は、渡したキーの値のハッシュをマージすること" do
        is_expected.to match({
          a: "A1", b: "B2", c: "C2", q: "Q2",
          search: {title_cont: "Art", episode_gt: 5, episode_lteq: 12},
        })
      end
    end
  end

  describe "#url_parameter_merge!" do
    subject do
      owner.url_parameter_merge!(other)
    end

    let(:owner){ {a: "A1", b: "B1", q: {medium_eq: "Anime", date_lt: "2019-01-07"}} }
    let(:other){ {b: "B2", c: "C2", q: {medium_eq: "Manga", date_gteq: "2018-12-25"}} }

    it "は、渡したハッシュを自身に破壊的にマージすること" do
      subject
      expect(owner).to match({
        a: "A1", b: "B2", c: "C2",
        q: {medium_eq: "Manga", date_lt: "2019-01-07", date_gteq: "2018-12-25"},
      })
    end

    it "は、自身を返すこと" do
      is_expected.to equal(owner)
    end
  end
end
