module CoreExt
  module Hash

    # HTML属性用のマージ機能（破壊的マージ）
    # @param [Hash] other マージするHTML属性群
    # @param [Boolean] compact 空の値を持つキーを省略するかどうか
    # @option other [String,Array<String>] :class クラス属性
    # @option other [Hash] :data データ属性
    # @return [Hash] マージ済みの自身
    def html_attribute_merge!(other, compact: true)
      merge!(
        other,
        class: Array(self[:class]) + Array(other[:class]),
        data: fetch(:data, {}).merge(other.fetch(:data, {})),
      )
      delete(:class) if compact && self[:class].join == ""
      delete(:data) if compact && self[:data].values.join == ""
      self
    end

    # URLパラメータ用のマージ機能（破壊的マージ）
    # @param [Hash] other マージするURLパラメータ群
    # @param [Boolean] compact 空の値を持つキーを省略するかどうか
    # @param [Symbol] query_key 検索クエリ情報を値とするキー
    # @option other [Hash] :q 検索クエリ情報。キーは`query_key`で変更可。
    # @return [Hash] マージ済みの自身
    def url_parameter_merge!(other, compact: true, query_key: :q)
      query_key = query_key.to_sym
      merge!(
        other,
        query_key => fetch(query_key, {}).merge(other.fetch(query_key, {})),
      )
      delete(query_key) if compact && self[query_key].values.join == ""
      self
    end

    %w(html_attribute_merge url_parameter_merge).each do |method_name|

      # 非破壊的マージ
      # @return [Hash] マージ済みハッシュ
      define_method(method_name) do |*args, &block|
        dup.send(method_name + "!", *args, &block)
      end
    end
  end
end
